const express = require("express");
const router = express.Router();
const middle = require("../data/middle");
const users = require("../data/users");
const product = require("../data/products");



/**
 * @swagger
 * /products:
 *  get:
 *    tags: 
 *      [Product]
 *    summary: Mostrar productos 
 *    description: Muestra todos los productos (requiere productos creados)
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/products", (req, res, next) => {
    res.json(product.products);
});

/**
 * @swagger
 * /user/newProduct:
 *  post:
 *    tags: 
 *      [Product]
 *    summary: Crea un producto
 *    description: Crea un nuevo producto y lo almacena en el array "products"
 *    parameters:
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: subname
 *      description: Nombre abreviado del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Producto creado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


router.post("/user/newProduct", (req, res, next) => {

    console.log(req.body.price);
    let aux = middle.validateADM(
        { username: req.body.admin, password: req.body.password },
        "",
        {
            name: req.body.name,
            subname: req.body.subname,
            price: req.body.price
        },
        "",
        "createNewProduct",
        "product");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.table(product.products);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/modProduct/{id}:
 *  put:
 *    tags: 
 *      [Product]
 *    summary: Modificar producto
 *    description: Modifica un  producto ya exitente requiere cuenta nivel admin
 *    parameters:
 *    - name: id
 *      description: Id producto a modificar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: Nombre del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: subname
 *      description: Nombre abreviado del producto
 *      in: formData
 *      required: false
 *      type: string
 *    - name: price
 *      description: Precio del producto
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Producto creado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


router.put("/user/modProduct/:id", (req, res, next) => {

    let aux = middle.validateADM(
        {
            username: req.body.admin,
            password: req.body.password
        },
        {
            name: req.body.name,
            subname: req.body.subname,
            price: req.body.price
        },
        parseInt(req.params.id),
        "",
        "modifyProduct",
        "product"
    );

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.table(product.products);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/dltProduct/{id}:
 *  delete:
 *    tags: 
 *      [Product]
 *    summary: Eliminar producto
 *    description: Elimina un producto ya existente
 *    parameters:
 *    - name: id
 *      description: Id del producto
 *      in: path
 *      required: true
 *      type: integer
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        description: Producto eliminado con exito
 *      404:
 *        description: Usuario no encontrado
 *      403:
 *        description: Contraseña incorrecta
 * 
 */

router.delete("/user/dltProduct/:id", (req, res, next) => {


    let aux = middle.validateADM(
        {
            username: req.body.admin,
            password: req.body.password
        },
        "",
        parseInt(req.params.id),
        "",
        "deleteProduct",
        "product"
    );

    if (aux.arr !== undefined && aux.arr.length !== 0) {
        product.products = aux.arr;
    }
    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA DELETE---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.table(product.products);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

module.exports = router;