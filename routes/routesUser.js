
const express = require("express");
const router = express.Router();
const middle = require("../data/middle");
const users = require("../data/users");

let contador = 0; //<<<<<< BORRAR


/**
 * @swagger
 * /users:
 *  get:
 *    tags: 
 *      [User]
 *    summary: Mostrar usuarios logeados 
 *    description: Muestra todos los usuarios actualmente logeados
 *    responses:
 *      200:
 *        description: Success
 */
 router.get("/users", (req, res, next) => {

    let aux = middle.show(true,users.clients,"state");
    console.log(aux);

    res.send("Usuarios conectados: " + aux);
});

/**
 * @swagger
 * /signUp:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Crear usuario
 *    description: Crea un usuario y lo almacena en el array "clients"
 *    parameters:
 *    - name: username
 *      description: Nickname del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: Teléfono del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Correo electrónico del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: address
 *      description: Domicilio del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario creado exitosamente
 *            409:
 *                description: Error al validar los datos 
 * 
 */


router.post("/signUp", (req, res, next) => {

    let aux = middle.creator(middle.clearData(req.body, "user"), users.clients, "user");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ${contador = contador + 1}---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(users.clients);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});


/**
 * @swagger
 * /logIn:
 *  post:
 *    tags: 
 *      [User]
 *    summary: Logear usuario
 *    description: Cambia el estado de un usuario de "false" a "true"
 *    parameters:
 *    - name: username
 *      description: Nickname del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario logeado exitosamente
 *            404:
 *                description: Usuario no encontrado
 *            403:
 *                description: Contraseña incorrecta 
 * 
 */


router.post("/logIn", (req, res, next) => {

    let aux = middle.seeker(req.body, users.clients, "user");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA LOGIN  ${contador = contador + 1}---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(users.clients);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/dltUser/{id}:
 *  delete:
 *    tags: 
 *      [User]
 *    summary: Eliminar usuario
 *    description: Da de baja a un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Usuario eliminado con exito
 *      404:
 *        description: Usuario no encontrado
 *      403:
 *        description: Primero debes logearte
 * 
 */

router.delete("/user/dltUser/:id", (req, res, next) => {
    
    let aux = middle.eraser(parseInt(req.params.id), users.clients, "user");
    

    if (aux.arr.length !== 0) {
        users.clients = aux.arr;
    }

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA DELETE  ${contador = contador + 1}---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(users.clients);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});


/**
 * @swagger
 * /user/modUser/{id}:
 *  put:
 *    tags: 
 *      [User]
 *    summary: Modificar usuario
 *    description: Modifica los datos de un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: true
 *      type: integer
 *    - name: username
 *      description: Nickname del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: name
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: phone
 *      description: Teléfono del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: email
 *      description: Correo electrónico del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: address
 *      description: Domicilio del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: Contraseña del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Usuario modificado exitosamente
 *            404:
 *                description: Usuario no encontrado
 *            403:
 *                description: Primero debes logearte
 * 
 */

router.put("/user/modUser/:id", (req, res, next) => {

    let aux = middle.editor(req.body, parseInt(req.params.id), users.clients, "user");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ${contador = contador + 1}---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(users.clients);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);


    next();
});

/**
 * @swagger
 * /user/rights: 
 *  post:
 *    tags: 
 *      [User]
 *    summary: Cambiar privilegios
 *    description: Modifica los permisos de un usuario
 *    parameters:
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: serial
 *      description: Número de serial de software
 *      in: formData
 *      required: true
 *      type: string
 *    - name: username
 *      description: Nombre de usuario o email de la persona a cambiar.
 *      in: formData
 *      required: true
 *      type: string
 *    - name:  rights
 *      description: Nivel de privilegios del usuario 
 *      in: query
 *      type: string
 *      enum: [client,adm]
 *      required: true
 *    responses:
 *            200:
 *                description: Permiso modificado con exito
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.post("/user/rights", (req, res, next) => {

    let aux = middle.validateADM(
        { username: req.body.admin, password: req.body.password },
        { username: req.body.username },
        req.body.serial,
        "",
        "changeRights",
        req.query.rights);

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ${contador = contador + 1}---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(users.clients);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);

    next();
});

module.exports = router;