const express = require("express");
const router = express.Router();
const middle = require("../data/middle");
const users = require("../data/users");
const pay = require("../data/payments");

/**
 * @swagger
 * /payments:
 *  post:
 *    tags: 
 *      [Payment]
 *    summary: Mostrar metodos de pago
 *    description: Muestra todos los metodos de pago
 *    parameters:
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Mostrar los metodos de pagos registrados
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */
router.post("/payments", (req, res, next) => {
    let aux = middle.validateADM({
        username: req.body.admin, password: req.body.password
    }, "", "", "", "showPay", "");

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /payments/newPay:
 *  post:
 *    tags: 
 *      [Payment]
 *    summary: Crear metodo de pago
 *    description: Crea un nuevo metodo de pago
 *    parameters:
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: Nombre del metodo de pago
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *            200:
 *                description: Permiso modificado con exito
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */
router.post("/payments/newPay", (req, res, next) => {

    let aux = middle.validateADM(
        {
            username: req.body.admin,
            password: req.body.password
        },
        "",
        { name: req.body.name },
        "",
        "createPay",
        "pay");


    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(pay.payments);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);
    next();

});

/**
 * @swagger
 * /payments/modPay/{id}:
 *  put:
 *    tags: 
 *      [Payment]
 *    summary: Modificar metodo de pago
 *    description: Modifica un metodo de pago ya existente
 *    parameters:
 *    - name: id
 *      description: Id metodo de pago a modificar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: Nombre del metodo de pago
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Metodo de pago modificado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


 router.put("/payments/modPay/:id", (req, res, next) => {

    let aux = middle.validateADM(
        {
            username: req.body.admin,
            password: req.body.password
        },
        {
            name: req.body.name,
        },
        parseInt(req.params.id),
        "",
        "modifyPay",
        "pay"
    );

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.table(pay.payments);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});


/**
 * @swagger
 * /payments/dltPay/{id}:
 *  delete:
 *    tags: 
 *      [Payment]
 *    summary: Eliminar metodo de pago
 *    description: Elimina un metodo de pago
 *    parameters:
 *    - name: id
 *      description: Id del metodo de pago
 *      in: path
 *      required: true
 *      type: integer
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        description: Metodo de pago eliminado con exito
 *      404:
 *        description: Usuario no encontrado
 *      403:
 *        description: Contraseña incorrecta
 * 
 */

router.delete("/payments/dltPay/:id", (req, res, next) => {

    //REUTILIZA PRODUCT
    let aux = middle.validateADM(
        {
            username: req.body.admin,
            password: req.body.password
        },
        "",
        parseInt(req.params.id),
        "",
        "deletePay",
        "product"
    );

    if (aux.arr !== undefined && aux.arr.length !== 0) {
        pay.payments = aux.arr;
    }

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA DELETE---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.table(pay.payments);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});


module.exports = router;
