const express = require("express");
const router = express.Router();
const middle = require("../data/middle");
const users = require("../data/users");
const pay = require("../data/payments");
const product = require("../data/products");

/**
 * @swagger
 * /user/newOrder:
 *  post:
 *    tags: 
 *      [Order]
 *    summary: Crear pedido
 *    description: Crea un nuevo pedido y lo almacena en el historial del cliente y en el array general de pedidos
 *    parameters:
 *    - name: username
 *      description: Nombre de usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: address
 *      description: Dirección de envío
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: name
 *      description: Nombre del metodo de pago
 *      in: formData
 *      required: true
 *      type: string 
 *    - name: p1
 *      description: Nombre del producto N° 1
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt1
 *      description: Cantidad deseada (producto N° 1)
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p2
 *      description: Nombre del producto N° 2
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt2
 *      description: Cantidad deseada (producto N° 2)
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p3
 *      description: Nombre del producto N° 3
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt3
 *      description: Cantidad deseada (producto N° 3)
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p4
 *      description: Nombre del producto N° 4
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt4
 *      description: Cantidad deseada (producto N° 4)
 *      in: formData
 *      required: false
 *      type: integer 
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.post("/user/newOrder", (req, res, next) => {
    console.log(users.clients);

    let aux = middle.newOrder(
        [{
            username: req.body.username,
            password: req.body.password,
            address: req.body.address,
            name: req.body.name
        },
        { name: req.body.p1, tlt: req.body.tlt1 },
        { name: req.body.p2, tlt: req.body.tlt2 },
        { name: req.body.p3, tlt: req.body.tlt3 },
        { name: req.body.p4, tlt: req.body.tlt4 }
        ]);

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    console.log(product.ordersTlt);
    console.log(users.clients);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);


    next();

});


/**
 * @swagger
 * /user/Orders:
 *  post:
 *    tags: 
 *      [Order]
 *    summary: Mostrar pedidos personales
 *    description: Muestra todos los pedidos realizados por un usuario
 *    parameters:
 *    - name: username
 *      description: Nombre de usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña 
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Muestra todos sus pedidos
 *            404:
 *                description: Usuario o producto no encontrado
 *            400:
 *                description: Error al validar los datos
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.post("/user/Orders", (req, res, next) => {

    let aux = middle.seeker(
        {
            username: req.body.username,
            password: req.body.password
        },
        users.clients, "user");

    (aux.err.reco !== undefined) ? (aux.err = aux.err.reco) : ("");
    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);


    next();

});

/**
 * @swagger
 * /user/allOrders:
 *  post:
 *    tags: 
 *      [Order]
 *    summary: Mostrar todos los pedidos 
 *    description: Muestra todos los pedidos realizados por todos los usuarios
 *    parameters:
 *    - name: username
 *      description: Nombre de administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña de administrador
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Muestra todos los pedidos
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */

router.post("/user/allOrders", (req, res, next) => {

    let aux = middle.validateADM(
        {
            username: req.body.username,
            password: req.body.password,
        },
        "",
        "",
        "",
        "showAllOrders",
        "");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);


    next();

});


/**
 * @swagger
 * /user/modOrderSts/{id}:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Modificar el estado de un pedido
 *    description: Modifica el estado de un pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido a modificar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: admin
 *      description: Nombre de usuario o email del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña del administrador
 *      in: formData
 *      required: true
 *      type: string
 *    - name:  stade
 *      description: Nuevo estado del pedido
 *      in: query
 *      type: string
 *      enum: [Preparando,Enviado,Entregado]
 *      required: true
 *    responses:
 *            200:
 *                description: Estado del pedido modificado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


router.put("/user/modOrderSts/:id", (req, res, next) => {

    let aux = middle.validateADM(
        { username: req.body.admin, password: req.body.password },
        "",
        req.params.id,
        "",
        "modifyOrderStade",
        req.query.stade);


    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/comfirmOrder/{id}:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Comfirmar pedido
 *    description: Comfirma y cierra el pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido a comfirmar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: username
 *      description: Nombre de usuario o email
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido comfirmado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


router.put("/user/comfirmOrder/:id", (req, res, next) => {

    let aux = middle.orderConfDelet(req.body,req.params.id,"Comfirmado");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/cancelOrder/{id}:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Cancelar pedido
 *    description: Cancela un pedido
 *    parameters:
 *    - name: id
 *      description: Id del pedido a cancelar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: username
 *      description: Nombre de usuario o email
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *            200:
 *                description: Pedido cancelado exitosamente
 *            409:
 *                description: Error al validar los datos 
 *            404:
 *                description: Usuario Administrador no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 * 
 */


 router.put("/user/cancelOrder/:id", (req, res, next) => {

    let aux = middle.orderConfDelet(req.body,req.params.id,"Cancelado");

    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA CREACION ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Request Status: ${aux.err}`);
    //SOLO VERIFICACION -----

    res.status(aux.status).send(aux.err);
    next();
});

/**
 * @swagger
 * /user/modOrder/{id}:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Modifica un pedido
 *    description: Modifica un pedido no comfirmado
 *    parameters:
 *    - name: id
 *      description: Id del pedido a cancelar
 *      in: path
 *      required: true
 *      type: integer
 *    - name: username
 *      description: Nombre de usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: Contraseña 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: address
 *      description: Dirección de envío
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: name
 *      description: Nombre del metodo de pago
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: p1
 *      description: Nombre del prodcuto a modificar/agregar
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt1
 *      description: Cantidad deseada
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p2
 *      description: Nombre del prodcuto a modificar/agregar
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt2
 *      description: Cantidad deseada
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p3
 *      description: Nombre del prodcuto a modificar/agregar
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt3
 *      description: Cantidad deseada
 *      in: formData
 *      required: false
 *      type: integer 
 *    - name: p4
 *      description: Nombre del prodcuto a modificar/agregar
 *      in: formData
 *      required: false
 *      type: string 
 *    - name: tlt4
 *      description: Cantidad deseada
 *      in: formData
 *      required: false
 *      type: integer 
 *    responses:
 *            200:
 *                description: Pedido exitoso
 *            404:
 *                description: Usuario o producto no encontrado
 *            403:
 *                description: Contraseña Incorrecta
 *            400:
 *                description: Error al validar los datos
 */



 router.put("/user/modOrder/:id", (req, res, next) => {

    let aux = middle.editor(req.body,req.params.id,"","order");


    //SOLO VERIFICACION -----
    console.log(`-------------------------PRUEBA MODIFICAR  ---------------------------`);
    console.log(`Server Status: ${aux.status}`);
    console.log(`Requests Status: ${aux.err}`);
    //SOLO VERIFICACION -----
    res.status(aux.status).send(aux.err);


    next();
});



module.exports = router;