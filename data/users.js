

let clients = [];

class User {
    constructor(username, name, phone, email, address, password, id, range, reco, state) {
        this.username = username;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.password = password;
        this.id = id;
        this.range = range;
        this.reco = reco;
        this.state = state;
    }
}

clients.push(new User("admin", "", "", "", "", "u90reu12jd", 1, "adm", [], false));
clients.push(new User("Lucas", "Lucas ", "16145414", "lucas@gmail.com", "Ushuaia 145", "123", 2, "client", [], true));
clients.push(new User("Pedro", "Pedro ", "16145414", "pedro@gmail.com", "San Martin 254", "345", 3, "adm", [], false));
clients.push(new User("Maria", "Maria ", "16145414", "maria@gmail.com", "Ituzango 524", "678", 4, "client", [], false));
clients.push(new User("Rosaria", "Rosaria ", "16145414", "rosaria@gmail.com", "Planetario", "asd2", 5, "client", [], true));






module.exports = {
    User,
    clients
}