const payments = [];

class Payment {
    constructor(name, id) {
        this.name = name;
        this.id = id;
    }
}

payments.push(new Payment("Tarjeta de Credito",1234));
payments.push(new Payment("Tarjeta de Debito",412));

module.exports = {
    Payment,
    payments
}