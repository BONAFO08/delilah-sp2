let products = [];
let ordersTlt = [];

class Product {
    constructor(name, subname, id, price) {
        this.name = name;
        this.subname = subname;
        this.id = id;
        this.price = price;
    }
}

products.push(new Product("Pizza", "Pizza", 124, 546));
products.push(new Product("Pan de Melon", "MelPan", 234, 124));
products.push(new Product("Kinako Pan", "KinPan", 225, 775));
products.push(new Product("Torta selva negra", "BlaForst", 141, 982));

module.exports = {
    Product,
    products,
    ordersTlt
}