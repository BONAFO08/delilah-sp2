const users = require("./users");
const serial = "asa5sd1458sd4f8";
const product = require("./products");
const pay = require("../data/payments");

const noSpace = (string) => {
    if (string !== undefined) {
        let aux = string.replace(/\s/g, "");
        return aux;
    }
};

const noAccent = (str) => {
    str = str.replace("á", "a");
    str = str.replace("é", "e");
    str = str.replace("í", "i");
    str = str.replace("ó", "o");
    str = str.replace("ú", "u");
    return str;
};

const noNumber = (str) => {
    str = str.replace(/1|2|3|4|5|6|7|8|9|0/g, "");
    return str;
};

const newDate = () => {
    let day = new Date();
    let month = new Date();
    let year = new Date();
    let hour = new Date();
    let min = new Date();


    let fullDate = {
        day: day.getDate(),
        month: month.getMonth() + 1,
        year: year.getFullYear(),
        hour: hour.getHours(),
        min: min.getMinutes()
    };
    return fullDate;
};

const noRepeat = (data, arr, paramIns, paramExt) => {
    msj = {
        err: "",
        status: 0,
        bool: true
    }
    switch (paramExt) {

        case "user":
            switch (paramIns) {
                //PARAMETROS INTERNOS USUARIO
                case "username":
                    if (data !== undefined) {
                        data.username = noSpace(data.username);
                        if (comparator(data, arr, paramIns) === false) {
                            return msj;
                        } else {
                            msj.err = "nombre de usuario";
                            msj.bool = false;
                            return msj;
                        }
                    } else {
                        return msj;
                    }

                case "email":
                    if (data !== undefined) {
                        data.email = noSpace(data.email);
                        if (comparator(data, arr, paramIns) === false) {
                            return msj;
                        } else {
                            msj.err = "correo electrónico";
                            msj.bool = false;
                            return msj;
                        }
                    } else {
                        return msj;
                    }
            }
            break;

        case "product":
            switch (paramIns) {
                //PARAMETROS INTERNOS PRODUCTOS
                case "producName":
                    if (data.name !== undefined) {
                        data.name = data.name.trim();
                        if (comparator(data, arr, paramIns) === false) {
                            return msj;
                        } else {
                            msj.err = "nombre del producto";
                            msj.bool = false;
                            return msj;
                        }
                    } else {
                        return msj;
                    }

                case "producSubname":
                    if (data.subname !== undefined) {
                        data.subname = noSpace(data.subname);
                        if (comparator(data, arr, paramIns) === false) {
                            return msj;
                        } else {
                            msj.err = "el nombre abreviado del producto";
                            msj.bool = false;
                            return msj;
                        }
                    } else {
                        return msj;
                    }
            }
            break;
    }
};



const newId = () => {
    let num = Math.floor(Math.random() * 10000) + 1;
    return num;
};



const clearData = (data, param) => {
    switch (param) {
        case "user":
            data.username = noSpace(data.username);
            data.name = data.name.trim();
            data.email = data.email.trim();
            data.address = data.address.trim();
            data.password = noSpace(data.password);
            return data;
    }
};


const comparator = (data, arr, param) => {
    let aux;
    switch (param) {

        case "email":
            aux = arr.filter(arr => arr.email === data.email);
            if (aux.length == 0) {
                return false;
            } else {
                return aux[0];
            }


        case "username":
            aux = arr.filter(arr => arr.username === data.username);
            if (aux.length == 0) {
                return false;
            } else {
                return aux[0];
            }

        case "password":
            if (data.password === arr[data.index].password) {
                return true;
            } else {
                return false;
            }

        case "state":
            aux = arr.filter(arr => arr.state === data);
            if (aux.length == 0) {
                return false;
            } else {
                return aux;
            }

        case "producName":
            data.name = data.name.trim();
            aux = arr.filter(arr => arr.name === data.name);
            if (aux.length == 0) {
                return false;
            } else {
                return aux[0];
            }

        case "producSubname":
            data.subname = data.subname.trim();
            aux = arr.filter(arr => arr.subname === data.subname);
            if (aux.length == 0) {
                return false;
            } else {
                return aux[0];
            }

        case "id":
            aux = arr.filter(arr => arr.id == data);
            if (aux.length == 0) {
                return false;
            } else {
                return aux[0];
            }

    }
};

const show = (data, arr, param) => {
    let arrtem = comparator(data, arr, param);
    let aux = "";
    for (let i = 0; i < arrtem.length; i++) {

        if (i === arrtem.length - 1) {
            aux += `${arrtem[i].username}. `;
        } else {
            aux += `${arrtem[i].username}, `;
        }
    }
    return aux;
};

// CREADOR
const creator = (data, arr, param) => {

    let checker = true;
    let msj = {
        status: 0,
        err: "",
    };

    switch (param) {

        case "user":
            if (comparator(data, arr, "email") === false) {
                checker = checker && true;
            } else {
                checker = checker && false;
                msj.err += "El correo electrónico ya está en uso. ";
                msj.status = 409;
            }

            if (comparator(data, arr, "username") === false) {
                checker = checker && true;
            } else {
                checker = checker && false;
                msj.err += "El nombre de usuario ya está en uso. ";
                msj.status = 409;
            }


            if (checker === true) {
                arr.push(new users.User(
                    data.username,
                    data.name,
                    data.phone,
                    data.email,
                    data.address,
                    data.password,
                    newId(),
                    "client",
                    [],
                    false
                ));
                msj.err += "Usuario creado con éxito.";
                msj.status = 200;
                return msj;
            } else if (checker === false) {
                return msj;
            }

            break;


        case "product":
            if (comparator(data, arr, "producName") === false && comparator(data, arr, "producSubname") === false) {

                data.name = data.name.trim();
                data.subname = noSpace(data.subname);
                data.price = data.price.trim();

                arr.push(new product.Product(
                    data.name,
                    data.subname,
                    newId(),
                    parseInt(data.price)));

                msj.err = `El producto ${data.name} fue creado exitosamente.`;
                msj.status = 200;
                return msj;
            } else {
                msj.err = "El producto ya existe.";
                msj.status = 409;
                return msj;
            }

        case "pay":
            let dataNOacc = { name: noAccent(data.name) }
            let dataNOnum = { name: noNumber(data.name) }
            let dataNOall = { name: noAccent(noNumber(data.name)) }

            if (comparator(data, arr, "producName") === false &&
                comparator(dataNOacc, arr, "producName") === false &&
                comparator(dataNOnum, arr, "producName") === false &&
                comparator(dataNOall, arr, "producName") === false) {
                data.name = data.name.trim();
                arr.push(new pay.Payment(
                    data.name,
                    newId()));

                msj.err = `El metodo de pago ${data.name} fue creado exitosamente.`;
                msj.status = 200;
                return msj;
            } else {
                msj.err = "El metodo de pago ya existe.";
                msj.status = 409;
                return msj;
            }

    }
};


const seeker = (data, arr, param) => {

    data.username = noSpace(data.username);
    data.password = noSpace(data.password);

    let msj = {
        status: 0,
        err: ""
    };

    let aux;
    let compared;
    let comparePassword = {
        password: data.password,
        index: 0
    };

    switch (param) {
        case "user":
            aux = data.username.includes("@") && data.username.includes(".com");
            if (aux === true) {
                // ES UN EMAIL
                data["email"] = data.username;
                compared = comparator(data, arr, "email");
            } else {
                // ES UN USERNAME
                compared = comparator(data, arr, "username");
            }

            // COMPARADO
            if (compared === false) {
                msj.status = 404;
                msj.err = "Usuario no encontrado.";
                return msj
            } else {
                comparePassword.index = arr.indexOf(compared);
                aux = comparator(comparePassword, arr, "password");
                (aux === true)
                    ? (arr[comparePassword.index].state = true,
                        msj.err = arr[comparePassword.index],
                        msj.status = 200)
                    : (msj.status = 403,
                        msj.err = `Contraseña Incorrecta.`);
                return msj;
            }
    };
};

//BORRADOR
const eraser = (id, arr, param) => {
    let msj = {
        status: 0,
        err: "",
        arr: []
    };
    let aux;
    switch (param) {

        case "user":

            aux = arr.filter(arr => arr.id === id);
            if (aux.length === 0) {
                msj.err = "Usuario no encontrado";
                msj.status = 404;
                return msj;
            } else if (aux[0].state === false) {
                msj.err = "Primero debes logearte.";
                msj.status = 403;
                return msj;
            } else {
                arr = arr.filter(arr => arr.id !== id);
                msj.err = "Usuario borrado con exito.";
                msj.status = 200;
                msj.arr = arr;
                return msj;
            }

        case "product":
            aux = arr.filter(arr => arr.id === id);
            if (aux.length === 0) {
                msj.err = "Producto no encontrado";
                msj.status = 404;
                return msj;
            } else {
                arr = arr.filter(arr => arr.id !== id);
                msj.err = "Producto borrado con exito.";
                msj.status = 200;
                msj.arr = arr;
                return msj;
            }
    }
};

const verifyData = (data, param) => {
    if (data === undefined) {
        return false;
    } else {
        if (param == "A") {
            return noSpace(data);
        } else if (param == "B") {
            return data.trim();
        }
    }
};



const changeData = (dataNew, index, arr, paramIns, paramExt) => {

    let aux;

    switch (paramExt) {

        case "user":
            //PARAMETROS INTERNOS USUARIO
            switch (paramIns) {
                case "username":
                    if (dataNew !== false) {
                        arr[index].username = dataNew;
                    }
                    break;
                case "name":
                    if (dataNew !== false) {
                        arr[index].name = dataNew;
                    }
                    break;
                case "phone":
                    if (dataNew !== false) {
                        arr[index].phone = dataNew;
                    }
                    break;
                case "email":
                    if (dataNew !== false) {
                        arr[index].email = dataNew;
                    }
                    break;
                case "address":
                    if (dataNew !== false) {
                        arr[index].address = dataNew;
                    }
                    break;
                case "password":
                    if (dataNew !== false) {
                        arr[index].password = dataNew;
                    }
                    break;
                case "pay":
                    if (dataNew !== false) {
                        aux = pay.payments.filter(arr => arr.name === dataNew);
                        if (aux.length !== 0) {
                            arr[index].pay = dataNew;
                        } else {
                            break;
                        }
                    }
                    break;
            } break;


        case "product":
            //PARAMETROS PRODUCTO
            switch (paramIns) {
                case "name":
                    if (dataNew !== false) {
                        arr[index].name = dataNew;
                    }
                    break;
                case "subname":
                    if (dataNew !== false) {
                        arr[index].subname = dataNew;
                    }
                    break;
                case "price":
                    if (dataNew !== false) {
                        arr[index].price = parseInt(dataNew);
                    }
                    break;
            } break;
    }
}



const changeOrder = (data, arr, arr2) => {
    let aux;
    let aux2
    let index;

    if (data.name !== undefined && data.name !== "") {
        aux = arr.filter(arr => arr.name === data.name);
        if (aux.length !== 0) {
            index = arr.indexOf(aux[0]);
            if (data.tlt === undefined || data.tlt === 0) {
                aux2 = arr.filter(arr => arr.name !== data.name);
                arr = aux2;
                return aux2;
            } else {
                arr[index].tlt = data.tlt;
                aux2 = arr;
                return aux2;
            }
        } else {
            aux2 = product.products.filter(products => products.name === data.name);
            if (aux2.length === 0) {
                arr2.rejectedFood.push(data);
                return arr;
            } else {
                if (data.tlt === 0 || data.tlt === undefined) {
                    data.tlt = 1;
                }
                arr.push(data);
                return arr;
            }
        }
    }
    return arr
}

const editor = (data, id, arr, param) => {



    let msj = {
        status: 0,
        err: "",
        arr: ""
    };

    let aux;
    let aux2;
    let aux3;
    let orderUs;
    let index;
    let index2;
    let index3;


    switch (param) {

        case "user":
            aux = arr.filter(arr => arr.id === id);
            aux2 = noRepeat(data, arr, "username", param);
            aux3 = noRepeat(data, arr, "email", param);
            (aux2.bool === false && aux3.bool === false) ? (aux2.err = `${aux2.err} y ${aux3.err}`, aux3.err = "") : ("");

            if (aux.length === 0) {
                msj.err = "Usuario no encontrado";
                msj.status = 404;
                return msj;
            } else if (aux[0].state === false) {
                msj.err = "Primero debes logearte.";
                msj.status = 403;
                return msj;
            } else if ((aux2.bool && aux3.bool) === true) {
                // A = Usa el metodo noSpace()
                // B = Usa el motodo trim()

                index = arr.indexOf(aux[0]);
                changeData(verifyData(data.username, "A"), index, arr, "username", param);
                changeData(verifyData(data.name, "B"), index, arr, "name", param);
                changeData(verifyData(data.phone, "A"), index, arr, "phone", param);
                changeData(verifyData(data.email, "A"), index, arr, "email", param);
                changeData(verifyData(data.address, "B"), index, arr, "address", param);
                changeData(verifyData(data.password, "A"), index, arr, "password", param);
                msj.err = "Los datos solicitados han sido actualizados";
                msj.status = 200;
                return msj;
            } else {
                msj.err = `El ${aux2.err}${aux3.err} ya esta/n en uso`;
                msj.status = 409;
                return msj;
            }

        case "product":
            aux = arr.filter(arr => arr.id === id);

            aux2 = noRepeat(data, arr, "producName", param);
            aux3 = noRepeat(data, arr, "producSubname", param);

            (aux2.bool === false && aux3.bool === false) ? (aux2.err = `${aux2.err} y ${aux3.err}`, aux3.err = "") : ("");
            if (aux.length === 0) {
                msj.err = "Producto no encontrado";
                msj.status = 404;
                return msj;
            } else if ((aux2.bool && aux3.bool) === true) {

                // A = Usa el metodo noSpace()
                // B = Usa el motodo trim()

                index = arr.indexOf(aux[0]);
                changeData(verifyData(data.name, "B"), index, arr, "name", param);
                changeData(verifyData(data.subname, "A"), index, arr, "subname", param);
                changeData(verifyData(data.price, "A"), index, arr, "price", param);
                msj.err = "Los datos solicitados han sido actualizados";
                msj.status = 200;
                return msj;
            } else {
                msj.err = `El ${aux2.err}${aux3.err} ya esta/n en uso`;
                msj.status = 409;
                return msj;
            }

        case "pay":

            //REUTILIZA "PRODUCT" 
            aux = arr.filter(arr => arr.id === id);
            aux2 = noRepeat(data, arr, "producName", "product");
            if (aux.length === 0) {
                msj.err = "Metodo de pago no encontrado";
                msj.status = 404;
                return msj;
            } else if (aux2.bool === true) {

                // A = Usa el metodo noSpace()
                // B = Usa el motodo trim()

                index = arr.indexOf(aux[0]);
                //REUTILIZA "PRODUCT" 
                changeData(verifyData(data.name, "B"), index, arr, "name", "product");
                msj.err = "Los datos solicitados han sido actualizados";
                msj.status = 200;
                return msj;
            } else {
                msj.err = `El nombre "${data.name}" ya esta en uso`;
                msj.status = 409;
                return msj;
            }

        case "order":
            aux = seeker({ username: data.username, password: data.password }, users.clients, "user");
            index3 = users.clients.indexOf(aux.err); //INDEX USUARIO

            if (aux.status === 403) {
                aux.status = 404
            }
      

            if (aux.status !== 404 && comparator(id, aux.err.reco, "id") !== false) {
                orderUs = comparator(id, aux.err.reco, "id");
                index2 = users.clients[index3].reco.indexOf(orderUs);
                aux2 = product.ordersTlt.filter(arr => arr.id === orderUs.id);
                index = product.ordersTlt.indexOf(aux2[0]); // INDEX DE ORDEN (EN ARR TOTAL)
                if (aux2[0].stade === "Nuevo") {
                    // A = Usa el metodo noSpace()
                    // B = Usa el motodo trim()


                    //CAMBIA DATOS EN ALL
                    changeData(verifyData(data.address, "B"), index, product.ordersTlt, "address", "user");
                    changeData(verifyData(data.name, "B"), index, product.ordersTlt, "pay", "user");


                    aux3 = changeOrder({ name: data.p1, tlt: (data.tlt1 !== undefined) ? (Math.floor(Math.abs(data.tlt1))) : (undefined) }, product.ordersTlt[index].acceptedFood, product.ordersTlt[index]);
                    product.ordersTlt[index].acceptedFood = aux3;
                    aux3 = changeOrder({ name: data.p2, tlt: (data.tlt2 !== undefined) ? (Math.floor(Math.abs(data.tlt2))) : (undefined) }, product.ordersTlt[index].acceptedFood, product.ordersTlt[index]);
                    product.ordersTlt[index].acceptedFood = aux3;
                    aux3 = changeOrder({ name: data.p3, tlt: (data.tlt3 !== undefined) ? (Math.floor(Math.abs(data.tlt3))) : (undefined) }, product.ordersTlt[index].acceptedFood, product.ordersTlt[index]);
                    product.ordersTlt[index].acceptedFood = aux3;
                    aux3 = changeOrder({ name: data.p4, tlt: (data.tlt4 !== undefined) ? (Math.floor(Math.abs(data.tlt4))) : (undefined) }, product.ordersTlt[index].acceptedFood, product.ordersTlt[index]);
                    product.ordersTlt[index].acceptedFood = aux3;
                    aux = 0;
                    
                    for (let i = 0; i < product.ordersTlt[index].acceptedFood.length; i++) {
                        aux2 =product.products.filter(products => products.name === product.ordersTlt[index].acceptedFood[i].name);
                        console.log(aux2[0].price);
                        console.log(product.ordersTlt[index].acceptedFood[i].tlt);
                        aux = aux + aux2[0].price * product.ordersTlt[index].acceptedFood[i].tlt;
                    }

                    product.ordersTlt[index].amount = `$${aux}`;

                    if (product.ordersTlt[index].acceptedFood.length === 0) {
                        product.ordersTlt[index].stade = "Cancelado";
                    }

                    users.clients[index3].reco[index2] = product.ordersTlt[index];
                    
                    if(product.ordersTlt[index].stade === "Cancelado"){
                    msj.status = 400;
                    msj.err = "Orden cancelada.Razón:No hay alimentos validos en la solicitud.";
                    return msj;
                    }else{
                        msj.status = 200;
                        msj.err = "Orden modificado con exito.";
                        return msj;
                    }

                } else {
                    msj.status = 403;
                    msj.err = "No puedes modificar un pedido ya confirmado/cancelado";
                    return msj;
                }
            } else {
                msj.status = 404;
                msj.err = "Pedido no encontrado o usuario erroneo";
                return msj;
            }

    }
};

const changeRights = (dataChang, param, data) => {

    let msj = {
        status: 0,
        err: ""
    };

    let aux;
    let aux2;

    if (data === serial) {

        aux2 = dataChang.username.includes("@") && dataChang.username.includes(".com");

        (aux2 === true) ? (aux2 = "email", dataChang["email"] = dataChang.username) : (aux2 = "username");


        aux = comparator(dataChang, users.clients, aux2);


        if (aux !== false) {
            users.clients[users.clients.indexOf(aux)].range = param;
            msj.status = 200;
            msj.err = `Usuario ${aux.username} ha sido cambiado a ${param}`;
            return msj;
        } else {
            msj.status = 404;
            msj.err = "El usuario a modificar no existe.";
            return msj;
        }
    } else {
        msj.status = 403;
        msj.err = "Código serial incorrecto.";
        return msj;
    }
};

const validateADM = (user, dataChang, data, arr, operation, param) => {

    user.username = noSpace(user.username);
    user.password = noSpace(user.password);

    let msj = {
        status: 0,
        err: ""
    };

    let aux = seeker(user, users.clients, "user");
    let aux2;
    let index;
    let index2;

    if (aux.status === 200) {
        if (aux.err.range === "adm") {
            //OPERACIONES UNICAS DE ADM 
            switch (operation) {
                case "changeRights":
                    if (param === "client" || param === "adm") {
                        dataChang.username = noSpace(dataChang.username);
                        data = noSpace(data);
                        aux = changeRights(dataChang, param, data);
                    } else {
                        msj.err = `Error, el parámetro "rights" es invalido.`;
                        msj.status = 401;
                        aux = msj;
                    }
                    return aux;

                //PRODUCTS
                case "createNewProduct":
                    aux = creator(data, product.products, param);
                    return aux;

                case "modifyProduct":
                    aux = editor(dataChang, data, product.products, param);
                    return aux;

                case "deleteProduct":
                    aux = eraser(data, product.products, param);
                    return aux;

                //PAY
                case "showPay":
                    if (pay.payments.length !== 0) {
                        msj.err = pay.payments;
                    } else {
                        msj.err = "No se ha definido ningún metodo de pago aún";
                    }
                    msj.status = 200;
                    return msj;

                case "createPay":
                    aux = creator(data, pay.payments, param);
                    return aux;

                case "modifyPay":
                    aux = editor(dataChang, data, pay.payments, param);
                    return aux;

                case "deletePay":
                    //REUTILIZA PRODUCT 
                    aux = eraser(data, pay.payments, param);
                    if (aux.status === 200) {
                        aux.err = "Metodo de pago borrado con exito.";
                    } else {
                        aux.err = "Metodo de pago no encontrado";
                    }
                    return aux;

                //ORDERS
                case "showAllOrders":
                    msj.status = 200;
                    msj.err = product.ordersTlt;
                    return msj;

                case "modifyOrderStade":
                    aux = comparator(data, product.ordersTlt, "id");
                    if (aux !== false) {
                        //BUSCANDO EL INDEX DEL USUARIO AL QUE EL PEDIDO PERTENECE
                        aux2 = comparator(aux, users.clients, "username");
                        index = users.clients.indexOf(aux2);
                        //BUSCANDO EL INDEX DEL PEDIDO DENTRO DEL HISTORIAL DEL USUARIO
                        aux2 = comparator(data, aux2.reco, "id");
                        index2 = users.clients[index].reco.indexOf(aux2);
                        users.clients[index].reco[index2].stade = param;

                        //BUSCANDO EL PEDIDO EN EL ARR TOTAL DE PEDIDOS
                        index = product.ordersTlt.indexOf(aux);
                        product.ordersTlt[index].stade = param;

                        msj.status = 200;
                        msj.err = "Estado del pedido modificado exitosamente";
                        return msj;
                    } else {
                        msj.status = 404;
                        msj.err = "Pedido no encontrado";
                        return msj;
                    }

            }
        } else {
            msj.status = 403;
            msj.err = "Acceso denegado.";
            return msj;
        }
    } else {
        return aux;
    }
};

const newOrder = (data) => {
    let msj = {
        status: 0,
        err: ""
    };
    let aux = seeker(data[0], users.clients, "user");
    let amount = 0;
    let date = newDate();
    let time;
    let order;
    let index = users.clients.indexOf(aux.err);
    let arrAuxFoodExist = [];
    let arrAuxFoodnoExist = [];

    if (date.min <= 9) {
        date.min = `0${date.min}`;
    }

    if (date.hour <= 12) {
        time = `${date.hour}:${date.min} AM`;
    } else {
        time = `${date.hour - 12}:${date.min} PM`;
    }


    if (aux.status === 200) {



        order = {
            username: aux.err.username,
            name: aux.err.name,
            address: aux.err.address,
            phone: aux.err.phone,
            id: newId(),
            pay: "",
            stade: "Nuevo",
            acceptedFood: [],
            rejectedFood: [],
            amount: 0,
            date: `${date.day}/${date.month}/${date.year}   ${time}`,
        };

        (data[0].address !== undefined) ? (order.address = data[0].address) : ("");

        for (let i = 1; i < data.length; i++) {
            if (data[i].name === undefined) {
                continue;
            } else {

                aux = comparator(data[i], product.products, "producName");

                (data[i].tlt == "" || data[i].tlt == undefined || Math.floor(Math.abs(data[i].tlt)) == 0) ? (data[i].tlt = 1) : ("");

                if (aux === false) {
                    arrAuxFoodnoExist.push(data[i]);
                } else {
                    amount = amount + Math.floor(Math.abs(aux.price)) * Math.floor(Math.abs(data[i].tlt));
                    arrAuxFoodExist.push(data[i]);

                }
            }
        }

        if (arrAuxFoodExist.length === 0) {
            msj.status = 400;
            msj.err = "Orden cancelada.Razón:No hay alimentos validos en la solicitud.";
            return msj;
        } else if (comparator(data[0], pay.payments, "producName") == false) {
            msj.status = 404;
            msj.err = "Orden cancelada.Razón: Metodo de pago invalido";
            return msj;
        } else {
            order.pay = data[0].name;
            order.amount = `$${amount}`;
            order.acceptedFood = arrAuxFoodExist;
            order.rejectedFood = arrAuxFoodnoExist;
            msj.err = "Pedido aceptado.";
            msj.status = 200;
            users.clients[index].reco.push(order);
            console.log(index);
            product.ordersTlt.push(order);
            return msj
        }
    } else {
        return aux;
    }
};

const orderConfDelet = (data, id, param) => {
    let msj = {
        status: 0,
        err: ""
    };
    let aux;
    let orderUs;
    let index; // RECO >> PRODUCT
    let index2; // ORDERTL >> PRODUCT
    let index3; // USER INDEX

    aux = seeker(data, users.clients, "user");
    index3 = users.clients.indexOf(aux.err);




    if (aux.status !== 404 && comparator(id, aux.err.reco, "id") !== false) {
        orderUs = comparator(id, aux.err.reco, "id");

        switch (param) {

            case "Comfirmado":
                if (orderUs.stade == "Nuevo") {

                    index = aux.err.reco.indexOf(orderUs);
                    aux = comparator(id, product.ordersTlt, "id");
                    index2 = product.ordersTlt.indexOf(aux);

                    users.clients[index3].reco[index].stade = param;
                    product.ordersTlt[index2].stade = param;

                    msj.status = 200;
                    msj.err = "Estado del pedido confirmado exitosamente";
                    return msj;
                } else {
                    msj.status = 403;
                    msj.err = "No puedes moficar un pedido ya comfirmado/cancelado";
                    return msj;
                }

            //MODIFICAR
            case "Cancelado":
                if (orderUs.stade != "Entregado" && orderUs.stade != "Cancelado") {

                    index = aux.err.reco.indexOf(orderUs);
                    aux = comparator(id, product.ordersTlt, "id");
                    index2 = product.ordersTlt.indexOf(aux);

                    users.clients[index3].reco[index].stade = param;
                    product.ordersTlt[index2].stade = param;

                    msj.status = 200;
                    msj.err = "Estado del pedido cancelado exitosamente";
                    return msj;
                } else {
                    msj.status = 403;
                    msj.err = "No puedes cancelar un pedido ya cancelado/entregado";
                    return msj;
                }
        }

    }
    msj.status = 404;
    msj.err = "Pedido no encontrado o usuario erroneo";
    return msj;
};

module.exports = {
    noSpace, // <<<<<< BORRAR
    clearData,
    creator,
    comparator, //<<<< BORRAR
    seeker,
    eraser,
    editor,
    validateADM,
    show,
    newOrder,
    orderConfDelet,
};