

const express = require("express");
const app = express();
const path = require("path"); // <<< BORRAR
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
const middle = require("../data/middle"); // <<<< BORRAR
let users = require("../data/users"); // <<< BORRAR
let product = require("../data/products"); // <<< BORRAR
const pay = require("../data/payments"); // <<< BORRAR
const port = 3000;


//settings

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Delilah Resto App',
            version: '1.0.0'
        }
    },
    apis: ['./routes/routesUser.js',
        './routes/routesProducts.js',
        './routes/routesPayments.js',
        './routes/routesOrders.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));











//middle
app.use(require("../routes/routesUser.js"));
app.use(require("../routes/routesProducts"));
app.use(require("../routes/routesPayments"));
app.use(require("../routes/routesOrders"));


// BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR  BORRAR BORRAR  BORRAR 




// BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR BORRAR  BORRAR  BORRAR BORRAR  BORRAR 



app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

